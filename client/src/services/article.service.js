import http from "../http-common.js";

class ArticleDataService {
  getAllArticles() {
    return http.get("/articles");
  }

  getArticle(id, data) {
    return http.get(`/articles/${id}`, data);
  }

  getPublishedAndPublicArticles() {
      return http.get("/articles/public")
  }

  createArticle(data) {
    return  http.post("/articles/create-article", data);
  }

  updateArticle(id, data) {
    return  http.put(`/articles/edit/${id}`, data);
  }

  deleteArticle(id) {
    return  http.delete(`/articles/delete/${id}`);
  }

  deleteAllArticles() {
    return  http.delete(`/articles/delete/all`);
  }

  findArticleByTitle(title) {
    return  http.get(`/articles?title=${title}`);
  }
}

export default new ArticleDataService();