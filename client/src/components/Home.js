import React, { useState, useEffect } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import UserService from "../services/user.service";

import PublicEvents from "../components/EventsFeed.components/PublicEvent/PublicEvents.component";
import AddArticle from "../components/NewsFeed.components/add-article.component";
import EditArticle from "./NewsFeed.components/edit-article.component";
import PublicAndPublishedArticlesList from "../components/NewsFeed.components/public_and_published_articles-list.component";


const Home = () => {
  const [content, setContent] = useState("");

  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        setContent(response.data);
        console.log(response.data);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, []);

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>{content}</h3>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/home/articles/public" className="navbar-brand">
          Newsfeed
        </a>
        <div className="navbar-nav mr-auto">
        <li className="nav-item">
            <Link to={"/home/events"} className="nav-link">
              Events
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/home/articles/public"} className="nav-link">
              Articles
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/home/create-article"} className="nav-link">
              Create article
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/home/articles/edit/:id"} className="nav-link">
              update article
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
         <Route path={"/home/events"} component={PublicEvents} />
          <Route path={"/home/articles/public"} component={PublicAndPublishedArticlesList} />
          <Route path={"/home/create-article"} component={AddArticle} />
          <Route path="/home/articles/edit/:id" component={EditArticle} />
        </Switch>
      </div>
      </header>
      <div>

    </div>

    </div>
  );
};

export default Home;