import React, { useState, useEffect } from "react";
import forumImage from '../../assets/forum.png'



const About = () => {

    return(
        <>
            <div className="about">
                <h3>Keep in touch with all your communities, share and create articles and events</h3>

                <div> 
                    <p>What can you do ?</p>
                </div>

                <h4>1. Stay in touch with friends and associations</h4>

                <div className="blue-container">
                    <div className="box">
                        <img src={forumImage} />
                    </div>
                    <div className="box">
                        <p>Contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparu
                        </p>
                    </div>
                </div>

                <h4>2. Create and read articles about various subjects</h4>

                <div className="blue-container">
                    <div className="box">
                        <p>Contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparu
                        </p>
                    </div>
                    <div className="box">
                        <img src={forumImage} />
                    </div>

                </div>

                <h4>3. Create and participate to events for your community and beyond</h4>

                <div className="blue-container">
                    <div className="box">
                        <img src={forumImage} />
                    </div>
                    <div className="box">
                        <p>Contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparu
                        </p>
                    </div>
                </div>
                
                <h3>You just need to register !</h3>
                <div className="register-button">Register</div>
            </div>        
        </>
    )
}

export default About;