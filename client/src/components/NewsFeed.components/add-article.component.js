import React, { useState } from "react";
import ArticleDataService from "../../services/article.service";
import { Link } from "react-router-dom";



const AddArticle = () => {
    const initialArticleState = {
        id: null,
        title:"",
        description:"",
        category:"",
        is_published: true,
        is_public: true,
        is_comment_allowed: true
    };

    const [article, setArticle] = useState(initialArticleState);
    const [submitted, setSubmitted] = useState(0);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setArticle({ ...article, [name]: value});
    };

    const saveArticle = () => {
        var data = {
            title: article.title,
            description: article.description,
            category: article.category
        };

        ArticleDataService.createArticle(data)
        .then(response => {
            setArticle({
                id: response.data.id,
                title: response.data.title,
                description: response.data.description,
                category: response.data.category,
            });
            setSubmitted(1);
            console.log(response.data)
        })
        .catch(error => {
            console.log(error);
        });
    };

    const newArticle = () => {
        setArticle(initialArticleState);
        setSubmitted(0);
    };
     
    return(
        <div className="submit-form">
          <h1>Add article</h1>
        {submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={newArticle}>
              Add a new
            </button>
            <button className="btn">
              <Link to={"/home/articles/public"} className="nav-link">
                  Return to Newsfeed
              </Link>
            </button>

          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control"
                id="title"
                required
                value={article.title}
                onChange={handleInputChange}
                name="title"
              />
            </div>
  
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                required
                value={article.description}
                onChange={handleInputChange}
                name="description"
              />
            </div>

            <div className="form-group">
              <label htmlFor="Category">Choose article category</label>
              <select 
                 id="category"
                 value={article.category} 
                 onChange={handleInputChange}
                 name="category" >
                  <option value={null}>   </option>
                  <option value="series">Series</option>
                  <option value="concert">Concert</option>
                  <option value="people">People</option>
                  <option value="event_feedback">Event feedback</option>
                  <option value="musique">Musique</option> 
              </select>
            </div>
  
            <button onClick={saveArticle} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
};

export default AddArticle;