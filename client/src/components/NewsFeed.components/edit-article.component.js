import React, { useState, useEffect } from "react";
import ArticleDataService from "../../services/article.service";

const EditArticle = props => {
  const initialArticleState = {
    id: null,
    title: "",
    description: "",
    is_published: false,
    is_public: false,
    is_comment_enabled : false
  };
  const [currentArticle, setCurrentArticle] = useState(initialArticleState);
  const [message, setMessage] = useState("");

  const getArticle = id => {
    ArticleDataService.getArticle(id)
      .then(response => {
        setCurrentArticle(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getArticle(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setCurrentArticle({ ...currentArticle, [name]: value });
  };
  

  // Allow to change publication status of an article
  const updatePublished = publicationStatus => {
    var data = {
      id: currentArticle.id,
      title: currentArticle.title,
      description: currentArticle.description,
      is_published: publicationStatus
    };

    ArticleDataService.updateArticle(currentArticle.id, data)
      .then(response => {
        setCurrentArticle({ ...currentArticle, is_published: publicationStatus });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

   // Allow to change privacy status of an article (article can be seen by all or by list of friends)
  const updatePrivacyStatus = privacyStatus => {
    var data = {
      id: currentArticle.id,
      title: currentArticle.title,
      description: currentArticle.description,
      is_public: privacyStatus
    };

    ArticleDataService.updateArticle(currentArticle.id, data)
      .then(response => {
        setCurrentArticle({ ...currentArticle, is_public: privacyStatus });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

     // Allow to change comment enable of an article (article can be commented or not)
     const updateCommentingStatus = commentingStatus => {
      var data = {
        id: currentArticle.id,
        title: currentArticle.title,
        description: currentArticle.description,
        is_comment_enabled: commentingStatus
      };
  
      ArticleDataService.updateArticle(currentArticle.id, data)
        .then(response => {
          setCurrentArticle({ ...currentArticle, is_comment_enabled: commentingStatus });
          console.log(response.data);
        })
        .catch(e => {
          console.log(e);
        });
    };

  const updateArticle = () => {
    ArticleDataService.updateArticle(currentArticle.id, currentArticle)
      .then(response => {
        console.log(response.data);
        setMessage("The article was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteArticle = () => {
    ArticleDataService.deleteArticle(currentArticle.id)
      .then(response => {
        console.log(response.data);
        props.history.push("/home/articles/public");
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <div>
      {currentArticle ? (
        <div className="edit-form">
          <h4>Edit Article</h4>
          <form>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control"
                id="title"
                name="title"
                value={currentArticle.title}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={currentArticle.description}
                onChange={handleInputChange}
              />
            </div>

            <div className="form-group">
              <label>
                <strong>Publication Status</strong>
              </label>
              {currentArticle.is_published ? "Published" : "Pending"}
            </div>

            <div className="form-group">
              <label>
                <strong>Privacy Status</strong>
              </label>
              {currentArticle.is_public ? "Public" : "Private"}
            </div>

            <div className="form-group">
              <label>
                <strong>Comments enable Status</strong>
              </label>
              {currentArticle.is_comment_enabled ? "Comments enabled" : "Comments not allowed"}
            </div>
          </form>

          {currentArticle.is_published ? (
            <button
              className="badge badge-warning mr-2"
              onClick={() => updatePublished(0)}
            >
              UnPublish
            </button>
          ) : (
            <button
              className="badge badge-info mr-2"
              onClick={() => updatePublished(1)}
            >
              Publish
            </button>
          )}

          {currentArticle.is_public ? (
            <button
              className="badge badge-warning mr-2"
              onClick={() => updatePrivacyStatus(0)}
            >
              Limit access to this Article to my friends
            </button>
          ) : (
            <button
              className="badge badge-info mr-2"
              onClick={() => updatePrivacyStatus(1)}
            >
              I want this article to be visible by all
            </button>
          )}

          {currentArticle.is_comment_enabled ? (
            <button
              className="badge badge-warning mr-2"
              onClick={() => updateCommentingStatus(0)}
            >
              I dont want my article to be commented
            </button>
          ) : (
            <button
              className="badge badge-info mr-2"
              onClick={() => updateCommentingStatus(1)}
            >
              I want my article to be commented
            </button>
          )}

          <button className="badge badge-danger mr-2" onClick={deleteArticle}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateArticle}
          >
            Update
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on an article Coucou...</p>
        </div>
      )}
    </div>
  );
};

export default EditArticle;