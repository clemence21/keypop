const db = require("../../../../config/sequelize.config");
const Article = db.articles;
const Comment = db.comments;
const Op = db.Sequelize.Op;


// Create and Save a new Article
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Title can not be empty!"
    });
    return;
  }

  // Create an Article
  const article = {
    title: req.body.title,
    description: req.body.description,
    category: req.body.category,
    is_published: req.body.is_published ? req.body.is_published : true,
    is_comment_enabled: req.body.is_comment_enabled ? req.body.is_comment_enabled : true,
    is_public: req.body.is_public ? req.body.is_public : true,
  };

  // Save Article in the database
  Article.create(article)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Article."
      });
    });
};

// Export and Save a new comment
exports.createComment = (articleId, comment) => {
    return Comment.create({
      name: comment.name,
      text: comment.text,
      articleId: articleId,
    })
      .then((comment) => {
        console.log("Comment was created" + JSON.stringify(comment, null, 4));
        return comment;
      })
      .catch((err) => {
        console.log("An error occured while creating comment: ", err);
      });
};

// Get comments for a given article
exports.findCommentsByArticleId = (articleId) => {
    return Article.findByPk(articleId, { include: ["comments"] })
      .then((article) => {
        return article;
      })
      .catch((err) => {
        console.log("An error occured while finding article: ", err);
      });
};

// Retrieve all Articles from the database where condition.
exports.findAllArticles = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Article.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving articles."
      });
    });
};

// Get comment for a given commentID
exports.findCommentById = (id) => {
    return Comment.findByPk(id, { include: ["article"] })
      .then((comment) => {
        return comment;
      })
      .catch((err) => {
        console.log("An Error occured while finding specific comment: ", err);
      });
};

// Retrieve all articles from the database including commments
exports.findAllArticlesWithComments = () => {
    return Article.findAll({
      include: ["comments"],
    }).then((articles) => {
      return articles;
    });
};

// Find a single article with an id 
exports.findOne = (req, res) => {
  const id = req.params.id;

  Article.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "mon article "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Article with id=" + id +'   '+ err
      });
    });
};

// Update a Article by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Article.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Article was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Article with id=${id}. Maybe Article was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Article with id=" + id
      });
    });
};


// Delete an article with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Article.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Article was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Article with id=${id}. Maybe Article was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Article with id=" + id
      });
    });
};


// Delete all Articles from the database.
exports.deleteAll = (req, res) => {
  Article.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Articles were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all articles."
      });
    });
};

// Find all published and public Articles 
exports.findAllPublishedAndPublic = (req, res) => {
  Article.findAll({ where: { is_published: true } && {is_public: true} })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published and public articles."
      });
    });
};

// Find all published Articles 
exports.findAllPublished = (req, res) => {
  Article.findAll({ where: { is_published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published articles."
      });
    });
};

// Find all public Articles 
exports.findAllPublic = (req, res) => {
  Article.findAll({ where: { is_public: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving public articles."
      });
    });
};