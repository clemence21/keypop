module.exports = (sequelize,  Sequelize) => {
    const Article = sequelize.define("article", {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        category: {
            type: Sequelize.ENUM('musique', 'concert', 'people', 'event feedback', 'series') 
        },
        is_published: {
            type: Sequelize.BOOLEAN
        },
        is_comment_enabled: {
            type: Sequelize.BOOLEAN
        },
        is_public: {
            type: Sequelize.BOOLEAN
        }
    })

    

    return Article;
};