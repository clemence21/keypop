module.exports = (sequelize,  Sequelize) => {
    const Event = sequelize.define("event", {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        date: {
            type: Sequelize.DATE
        },
        place: {
            type: Sequelize.STRING
        },
        is_published: {
            type: Sequelize.BOOLEAN
        },
        is_public: {
            type: Sequelize.BOOLEAN
        },
        is_outside: {
            type: Sequelize.BOOLEAN
        },
    })

    return Event;
};