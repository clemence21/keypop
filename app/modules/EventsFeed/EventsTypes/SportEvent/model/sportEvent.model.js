module.exports = (sequelize,  Sequelize) => {
    const SportEvent = sequelize.define("sport_event", {
        number_of_players: {
            type: Sequelize.INTEGER
        },
        type_of_match: {
            type: Sequelize.ENUM('official','friendly')
        }
    })

    return SportEvent;
};