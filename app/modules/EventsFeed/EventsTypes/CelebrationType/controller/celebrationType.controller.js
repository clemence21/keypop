const db = require("../../../../config/sequelize.config");
const CelebrationType = db.celebrationTypes;
const Op = db.Sequelize.Op;

// Create and Save a celebration type
exports.createCelebrationType = (req, res) => {
    // Create a celebration type
    const celebrationType = {
      name: req.body.name,
      description: req.body.description,
      max_participants: req.body.max_participants,
    };
  
    // Save celebration event in the database
    CelebrationType.create(celebrationType)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the celebration type."
        });
      });
  };

// Find a single celebration type with an id 
exports.findOneCelebrationTypeById = (req, res) => {
  const id = req.params.id;

  CelebrationType.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "my celebration type "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving celebration type with id=" + id +'   '+ err
      });
    });
};

// Update an event by the id in the request
exports.updateCelebrationTypeById = (req, res) => {
  const id = req.params.id;

  CelebrationType.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Celebration type was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update celebration Type ith id=${id}. Maybe this celebration type was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating celebration type with id=" + id
      });
    });
};

// Delete a celebration type with the specified id in the request
exports.deleteCelebrationTypeById = (req, res) => {
  const id = req.params.id;

  CelebrationType.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Celebration Type was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete celebration Type with id=${id}. Maybe celebration Type was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete celebration type with id=" + id
      });
    });
};

// Delete all celebration types from the database.
exports.deleteAllCelebrationTypes = (req, res) => {
  CelebrationType.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} celebration types were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all celebration types."
      });
    });
};


//// Find all celebration event whre max participant is 25
exports.findAllCelebrationEventsWithMaxParticipants50 = (req, res) => {
  CelebrationEvent.findAll({ where: {
     max_participants: {
      [Op.lte]: 25,
     } 
    } 
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving celebration types with 25 max participants"
      });
    });
};