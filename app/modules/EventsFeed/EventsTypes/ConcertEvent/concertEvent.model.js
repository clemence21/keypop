module.exports = (sequelize,  Sequelize) => {
    const ConcertEvent = sequelize.define("concert_event", {
        band: {
            type: Sequelize.STRING
        },
        band_url: {
            type: Sequelize.STRING
        },
        musique_type : {
            type: Sequelize.ENUM('Jazz', 'Rap', 'Chanson française')
        }
    })

    return ConcertEvent;
};