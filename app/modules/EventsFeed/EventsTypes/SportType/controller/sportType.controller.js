const db = require("../../../../config/sequelize.config");
const SportType = db.sportTypes;
const Op = db.Sequelize.Op;

// Create and Save a sport type
exports.createSportType = (req, res) => {
    // Create a sport type
    const sportType = {
      name: req.body.name,
      description: req.body.description,
      rule: req.body.rule,
    };
  
    // Save sport type in the database
    SportType.create(sportType)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the sport type."
        });
      });
  };

// Find a single sport type with an id 
exports.findOneSportTypeById = (req, res) => {
  const id = req.params.id;

  SportType.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "my sport type "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving sport type with id=" + id +'   '+ err
      });
    });
};

// Update a Sport type by the id in the request
exports.updateSportTypeById = (req, res) => {
  const id = req.params.id;

  SportType.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Sport type was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Sport Type ith id=${id}. Maybe this Sport type was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Sport type with id=" + id
      });
    });
};

// Delete a Sport type with the specified id in the request
exports.deleteSportTypeById = (req, res) => {
  const id = req.params.id;

  SportType.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Sport Type was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Sport Type with id=${id}. Maybe Sport Type was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Sport type with id=" + id
      });
    });
};

// Delete all Sporttypes from the database.
exports.deleteAllSportTypes = (req, res) => {
    SportType.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Sport types were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Sport types."
      });
    });
};


//// Find all Sport event whre max participant is 25
exports.findAllSportEventsWithMaxParticipants50 = (req, res) => {
    SportEvent.findAll({ where: {
     name: "football"
    } 
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Sport types with 25 max participants"
      });
    });
};