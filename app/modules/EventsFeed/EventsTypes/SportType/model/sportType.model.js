module.exports = (sequelize,  Sequelize) => {
    const SportType = sequelize.define("sport_type", {
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        rule: {
            type: Sequelize.STRING
        }
    })

    return SportType;
};