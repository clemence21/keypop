const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    port: config.PORT,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// DEFINE USER AND USER ROLES 
db.user = require("../modules/User/model/user.model")(sequelize, Sequelize);
db.role = require("../modules/User/model/role.model")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});

db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.ROLES = ["user", "admin", "moderator"];

db.profiles = require("../modules/Profile/model/profile.model")(sequelize, Sequelize);

db.profiles.belongsTo(db.user, {
  foreignKey: "userId",
  as: "user"
})

// DEFINE ARTICLES AND COMMENTS

db.articles = require("../modules/Newsfeed/Article/model/article.model")(sequelize, Sequelize);
db.comments = require("../modules/Newsfeed/Comment/model/comment.model")(sequelize, Sequelize);

db.articles.hasMany(db.comments, { as: "comments" });

db.comments.belongsTo(db.articles, {
  foreignKey: "articleId",
  as: "article",
})

// DEFINE EVENTS 

db.events = require("../modules/EventsFeed/Events/model/event.model")(sequelize, Sequelize);

db.sportEvents = require("../modules/EventsFeed/EventsTypes/SportEvent/model/sportEvent.model")(sequelize, Sequelize);
db.sportTypes = require("../modules/EventsFeed/EventsTypes/SportType/model/sportType.model")(sequelize, Sequelize);

db.concertEvents = require("../modules/EventsFeed/EventsTypes/ConcertEvent/concertEvent.model")(sequelize, Sequelize);

db.celebrationEvents = require("../modules/EventsFeed/EventsTypes/CelebrationEvent/model/celebrationEvent.model")(sequelize, Sequelize);
db.celebrationTypes = require("../modules/EventsFeed/EventsTypes/CelebrationType/model/celebrationType.model")(sequelize, Sequelize);


db.user.hasMany(db.events, { as: "events" });

db.events.belongsTo(db.user, {
  foreignKey: "userId",
  as: "Event_creator",
})

db.user.hasMany(db.user, { as: "user"});
db.user.belongsToMany(db.user, {
  as: "friends", 
  through: "friendship", 
  foreignKey: "userId", 
  otherKey: "friendId"
})

//
const User_Event = sequelize.define('User_Event', {
  participantHasConfirmed : Sequelize.DataTypes.BOOLEAN,
  'date' : Sequelize.DataTypes.DATE,
  'summary' : Sequelize.DataTypes.STRING,
  'title' : Sequelize.DataTypes.STRING,
});

db.events.belongsToMany(db.user, {
  through: User_Event,

});

db.user.belongsToMany(db.events, {
  through: User_Event,

});



db.user.hasMany(db.articles, { as: "articles" });

db.articles.belongsTo(db.user, {
  foreignKey: "userId",
  as: "Article_creator",
})

db.user.hasMany(db.comments, { as: "comments" });

db.comments.belongsTo(db.user, {
  foreignKey: "userId",
  as: "Comment_creator",
})


// DEFINE SPORT EVENTS
// Sports Events are events

db.sportEvents.belongsTo(db.events, {
  foreignKey: "eventId",
  as: "event",
})

db.sportTypes.hasMany(db.sportEvents, {as: "sportEvent"} );

db.sportEvents.belongsTo(db.sportTypes, {
  foreignKey: "sportTypeId",
  as: "sportType",
})

// DEFINE CONCERT EVENTS 
// One concert is a specific event
db.concertEvents.belongsTo(db.events, {
  foreignKey: "eventId",
  as: "event",
})

// DEFINE CELEBRATION EVENTS
// One celebration is a type of event
db.celebrationEvents.belongsTo(db.events, {
  foreignKey: "eventId",
  as: "event",
})

db.celebrationTypes.hasMany(db.celebrationEvents, {as: "celebrationEvent"} );

db.celebrationEvents.belongsTo(db.celebrationTypes, {
  foreignKey: "celebrationTypeId",
  as: "celebrationType",
})
module.exports = db;